<?php

namespace Database\Factories;

use App\Models\Pays;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaysFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pays::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'libelle' => $this->faker->country,
            'description' => $this->faker->sentence,
            'code_indicatif' => $this->faker->countryCode,
            'continent' => $this->faker->randomElement(["Afrique","Amerique","Asie","Océanie"]),
            'population' => $this->faker->randomElement([50000000,80000000,80000,688288278,920298229]),
            'capitale' =>$this->faker->city,
            'devise' => $this->faker->randomElement(["EURO","XOF","XAF","DOLLAR"]),
            'langue' => $this->faker->randomElement(["ANGLAIS","FRANCAIS","BAOULE","PORTUGAIS"]),
            'superficie' => $this->faker->randomElement([150000000,950000000,450000000,50000000,150000000,550000000,5880000000]),
            'laic' => $this->faker->randomElement(["OUI","NON"])
        ];
    }
}
