<div class="col-md-12">

    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Edit Profile</h4>
        <p class="card-category">Complete your profile</p>
      </div>
      <div class="card-body">
        <form action="{{route("pays.store")}}" method="POST">
            @method("POST")
                @csrf<!--empêche les injections  sql-->

          <div class="row">
            <div class="col-md-5">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Company (disabled)</label>
                <input type="text" class="form-control" disabled="">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">libelle</label>
                <input type="text" name="libelle" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">capitale</label>
                <input type="text" name="capitale" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">langue</label>
                <select id="cars" name="langue" class="form-control">
                    <option value=""></option>
                    <option value="anglais">Anglais</option>
                    <option value="francais">Francais</option>
                    <option value="allemand">Allemand</option>
                    <option value="espagnol">Espagnol</option>
                  </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">code indicatif</label>
                <input type="text" name="code_indicatif" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Description</label>
                <input type="text" name="description" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">devise</label>
                <select id="cars" name="devise" class="form-control">
                    <option value=""></option>
                    <option value="euro">EURO</option>
                    <option value="xaf">XAF</option>
                    <option value="xof">XOF</option>
                    <option value="dollar">DOLLAR</option>
                  </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">continent</label>
                <input type="text" name="continent" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Laicité</label>
                <select id="cars" name="laic" name="devise" class="form-control">
                    <option value=""></option>
                    <option value="oui">OUI</option>
                    <option value="non">NON</option>
                </select>
            </div>
            </div>
          </div>
          <div class="row">

            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">superficie</label>
                <input type="text" name="superficie" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Nombre d'habitant</label>
                  <input type="text" name="population" class="form-control">
                </div>
              </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>About Me</label>
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating"> Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</label>
                  <textarea class="form-control" rows="5"></textarea>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary pull-right">Enregistrer</button>
          <div class="clearfix"></div>
        </form>
      </div>
    </div>
  </div>

  </div>

