<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<!-- CSS Files -->
<link rel="stylesheet"  href="{{asset("../assets/css/material-dashboard.css?v=2.1.2")}}" />
<!-- CSS Just for demo purpose, don't include it in your project -->
<link rel="stylesheet" href="{{asset("../assets/demo/demo.css")}}">
<link rel="stylesheet" href="{{asset("fontawesome-free/css/all.min.css")}}">
