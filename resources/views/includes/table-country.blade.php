<div class="col-md-8">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title ">Pays</h4>
        <p class="card-category"> Informations pays </p>
      </div>
      <div class="card-body">
        <div class="table-responsive">





  <table class="table">

    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Libelle</th>
            <th>Description</th>
            <th>Indicatif</th>
            <th>Continent
             </th>
             <th>
                Population
             </th>
             <th>
                Capitale
             </th>
             <th>
                Devise
             </th>
             <th>
                Langue
             </th>
             <th>
                Superficie
             </th>
             <th>
                Etat Laique
             </th>
            <th class="text-right">Actions</th>

        </tr>
    </thead>
    <tbody>
        @foreach ($pays as $item)

        <tr>
            <td class="text-center">{{$item->id}}</td>
            <td>{{$item->libelle}}</td>
            <td>{{$item->description}}</td>
            <td>{{$item->code_indicatif}}</td>
            <td class="text-right">{{$item->continent}}</td>
            <td>{{$item->population}}</td>
            <td>{{$item->capitale}}</td>
            <td>{{$item->devise}}</td>
            <td>{{$item->langue}}</td>
            <td>{{$item->superficie}}</td>
            <td>{{$item->laic}}</td>

            <td class="td-actions text-right">
                <a href="{{url('/')}}"  class="btn btn-info" title="Visualiser"><i class="material-icons">person</i></a>
                <a href="{{url('/')}}"  class="btn btn-success" title="Modifier"> <i class="material-icons">edit</i></a>
                <a href="{{url('/')}}"  class="btn btn-danger " title="Supprimer">  <i class="material-icons">close</i></a>
            </td>
        </tr>

        @endforeach
           </tbody>
    </table>


        </div>
      </div>
    </div>
  </div>
