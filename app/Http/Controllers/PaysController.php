<?php

namespace App\Http\Controllers;

use App\Models\pays;
use Illuminate\Http\Request;

class PaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('Layouts.main',[
            /**
             *
             * produit: nom de la variable dans la page produit
             * Produit: nom du modele
             *
             *  */

            "pays"=> Pays::all()

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('Layouts.main'); //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //VAlidation de la req
        // valider les champs du formulaire
        $request->validate([

            "libelle"=>'required',
            'libelle' =>'required',
            'description' =>'required',
            'code_indicatif' =>'required',
            'continent' =>'required',
            'population' =>'required',
            'capitale' =>'required',
            'devise' =>'required',
            'langue' =>'required',
            'superficie' =>'required',
            'laic' =>'required'

        ]);

        // enregistrement dans la base de donnée

        $pays = Pays::create([

            'libelle' =>$request->get('libelle'),
            'description' =>$request->get('description'),
            'code_indicatif' =>$request->get('code_indicatif'),
            'continent' =>$request->get('continent'),
            'population' =>$request->get('population'),
            'capitale' =>$request->get('capitale'),
            'devise' =>$request->get('devise'),
            'langue' =>$request->get('langue'),
            'superficie' =>$request->get('superficie'),
            'laic' =>$request->get('laic'),

        ]) ;

        return redirect()->route("pays.store");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function show(pays $pays)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function edit(pays $pays)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pays $pays)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function destroy(pays $pays)
    {
        //
    }
}
